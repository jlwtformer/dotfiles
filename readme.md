# Jacob Westall's Dotfiles Repo

## Purpose

The purpose of this repository is to provide my configuration files to anyone who wishes to view how I setup my Linux powered machines. It is available to download and use for free by anyone.

## Screenshots

Below is a screenshot showing what my current configuration looks like.

![alt text](./screenshot.png "Screenshot")
